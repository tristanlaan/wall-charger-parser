# Alfen Eve wall charger log parser
This project contains a parsing script to convert a MyEve transaction data
export to CSV. Will additionally consult [https://api.energyzero.nl](https://api.energyzero.nl) for
transaction costs (see `energy_util.py`).

## Usage
```sh
python3 extract_power_usage.py -i <LOGFILE> -o <OUTDIR>
```
