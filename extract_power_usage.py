#!/usr/bin/env python3
# Alfen Eve wall charger log parser
# Copyright (C) 2023 Tristan Laan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import random
import re
import sys
import time
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from typing import Optional

import numpy as np
import time_util
import energy_util

from pathlib import Path
from dataclasses import dataclass
from datetime import datetime, date, timedelta


@dataclass
class MeterValue:
    timestamp: datetime
    meter_value: float


@dataclass
class Transaction:
    date: datetime
    duration: timedelta
    charged: float
    tag: str
    meter_values: list[MeterValue]

    def charged_per_hour(self) -> dict[datetime, float]:
        def to_hour(timestamp: datetime) -> datetime:
            return timestamp.replace(minute=0, second=0, microsecond=0)

        charged_per_hour = dict()
        prev_mv = None
        total_charge = 0.0
        current_charge = 0.0

        if len(self.meter_values) < 2:  # Edge case with too few elements
            charged_per_hour[to_hour(self.date)] = self.charged
            total_charge += self.charged

        for mv in self.meter_values:
            if prev_mv is None:  # First iteration
                prev_mv = mv
                continue

            # Check if we have passed an hour mark
            if to_hour(prev_mv.timestamp) != to_hour(mv.timestamp):
                # Assume we charged at a constant speed, spread charge over
                # previous and current hour based on amount of time before and
                # after the hour mark
                charge = mv.meter_value - prev_mv.meter_value
                duration = mv.timestamp - prev_mv.timestamp
                prev_hour = to_hour(mv.timestamp) - prev_mv.timestamp

                assert (prev_hour / duration) >= 0 \
                    and (prev_hour / duration) <= 1

                # Store previous hour charge in dict
                current_charge += (prev_hour / duration) * charge
                charged_per_hour[to_hour(prev_mv.timestamp)] = current_charge
                total_charge += current_charge

                # Reset current charge and add this hour charge
                current_charge = (1 - (prev_hour / duration)) * charge
            else:
                current_charge += mv.meter_value - prev_mv.meter_value

            prev_mv = mv

        if current_charge != 0.0:
            charged_per_hour[to_hour(mv.timestamp)] = current_charge
            total_charge += current_charge
            current_charge = 0.0

        if abs(self.charged - total_charge) > 0.01:
            print(
                f"Warning: lost {self.charged - total_charge} kWh in "
                f"calculation for charge session {self.date.isoformat()}",
                file=sys.stderr)

        return charged_per_hour

    def cost_per_hour(self) -> dict[datetime, tuple[float, float, float]]:
        cost_per_hour = dict()
        charge_per_hour = self.charged_per_hour()
        if len(charge_per_hour) == 0:
            return dict()

        price_per_hour = energy_util.get_energy_price(
            min(list(charge_per_hour.keys())),
            max(list(charge_per_hour.keys())))

        for date, charge in charge_per_hour.items():
            price = price_per_hour[date]
            cost_per_hour[date] = (charge, price, charge * price)

        return cost_per_hour


class Timeline:
    def __init__(self) -> None:
        self.transactions: dict[datetime, Transaction] = dict()

    def add_transaction(self, date: datetime, transaction: Transaction):
        if date in self.transactions:
            raise KeyError(f"{date} already has an associated transaction")
        self.transactions[date] = transaction

    @classmethod
    def create_from_logdict(cls, raw_data: dict):
        def meter_value_from_dict(meter_value):
            return MeterValue(
                time_util.parse_time(meter_value['time']),
                float(meter_value['meterValue']))

        timeline = cls()
        for transaction in raw_data:
            meter_values = []
            if transaction['tag'] != 'txstart':
                continue
            for mv in transaction['values']['values']:
                meter_values.append(meter_value_from_dict(mv))

            timestamp = datetime.fromisoformat(transaction['date'])
            new_transaction = Transaction(
                timestamp,
                time_util.parse_timedelta(
                    transaction['values']['duration']),
                float(transaction['values']['totalAmount']),
                transaction['values']['tag'],
                meter_values)
            try:
                timeline.add_transaction(timestamp, new_transaction)
            except KeyError as e:
                print(f"Warning: failed adding transaction: {e}",
                      file=sys.stderr)

        return timeline


def read_data(input) -> dict:
    raw_data = json.load(input)
    timeline = Timeline.create_from_logdict(raw_data)
    return timeline


def calculate_power_usage(timeline: Timeline):
    total_power_usage = dict()
    for transaction in timeline.transactions.values():
        power_usage = transaction.charged_per_hour()
        for hour, charge in power_usage.items():
            assert charge >= 0.0
            if charge == 0.0:
                continue

            if hour in total_power_usage:
                total_power_usage[hour] += charge
            else:
                total_power_usage[hour] = charge

    return total_power_usage


def calculate_charging_price_hour(timeline: Timeline):
    total_cost = dict()
    for i, transaction in enumerate(timeline.transactions.values()):
        costs_transaction = transaction.cost_per_hour()
        for hour, (charge, price, cost) in costs_transaction.items():
            assert charge >= 0.0
            if charge == 0.0:
                continue

            if hour in total_cost:
                tmp_charge = total_cost[hour][0] + charge
                tmp_cost = total_cost[hour][2] + cost
                total_cost[hour] = (tmp_charge, price, tmp_cost)
            else:
                total_cost[hour] = (charge, price, cost)

        print(
            f"calculated transaction {i + 1}/{len(timeline.transactions)}",
            file=sys.stderr)

    return total_cost


def calculate_charging_price_transaction(timeline: Timeline):
    total_cost = dict()
    for i, transaction in enumerate(timeline.transactions.values()):
        costs_transaction = transaction.cost_per_hour()
        charges = 0.0
        costs = 0.0
        for _hour, (charge, _price, cost) in costs_transaction.items():
            assert charge >= 0.0
            if charge == 0.0:
                continue

            charges += charge
            costs += cost

        if costs == 0.0:
            continue

        total_cost[transaction.date] = (charges, costs / charges, costs)

        print(
            f"calculated transaction {i + 1}/{len(timeline.transactions)}",
            file=sys.stderr)

    return total_cost


def convert_cost_to_csv(
        cost_per_hour: dict[datetime, tuple[float, float, float]]):
    string = 'timestamp,charged (kWh), price (€/kWh), cost (€)\n'
    for date, (charge, price, cost) in cost_per_hour.items():
        string += f"{date.isoformat()},{charge:.2f},{price:.2f},{cost:.2f}\n"
    return string


def plot_usage_month(cost_per_hour: dict[datetime, tuple[float, float, float]], output_file: Path):
    def to_month(date: datetime):
        return date.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
    months = []
    usages = []
    costs = []

    for date, data in cost_per_hour.items():
        month = to_month(date)
        if month not in months:
            months.append(month)
            usages.append(data[0])
            costs.append(data[2])
        else:
            usages[months.index(month)] += data[0]
            costs[months.index(month)] += data[2]

    fig = plt.figure(figsize=(10, 4))
    ax1 = fig.add_subplot(1, 1, 1)
    ax2 = ax1.twinx()
    ax1.bar(np.array(months) - timedelta(days=1), usages, width=2, color='tab:blue')
    ax1.set_xlabel("Date")
    ax1.xaxis.set_major_locator(mdates.MonthLocator(interval=1))
    ax1.set_ylabel("Charged (kWh)")
    ax1.tick_params(axis='y', labelcolor='tab:blue')
    ax2.bar(np.array(months) + timedelta(days=1), costs, width=2, color='tab:red')
    ax2.set_ylabel("Cost (€)")
    ax2.tick_params(axis='y', labelcolor='tab:red')

    fig.suptitle("Wall charger usage")
    fig.autofmt_xdate()
    fig.savefig(str(output_file))



if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description='Extract power usage per hour from Alfen pro wall charger log')
    parser.add_argument(
        '-i', '--input', type=Path, default=None,
        help='Input log file, will read from stdin if not specified.')
    parser.add_argument(
        '-o', '--output', type=Path, default=Path('output'),
        help='Output folder, %(default)s by default.')

    args = parser.parse_args()
    if args.input is None:
        timeline = read_data(sys.stdin)
    else:
        with args.input.open('r') as f:
            timeline = read_data(f)

    # power_usage_data = calculate_power_usage(timeline)
    hour_data = calculate_charging_price_hour(timeline)
    transaction_data = calculate_charging_price_transaction(timeline)
    hour_csv = convert_cost_to_csv(hour_data)
    transaction_csv = convert_cost_to_csv(transaction_data)

    args.output.mkdir(exist_ok=True)
    with (args.output / 'cost_per_hour.csv').open('w') as f:
        f.write(hour_csv)
    with (args.output / 'cost_per_transaction.csv').open('w') as f:
        f.write(transaction_csv)

    plot_usage_month(hour_data, args.output / 'plot_month.svg')
