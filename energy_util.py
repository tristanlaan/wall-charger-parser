# Alfen Eve wall charger log parser
# Copyright (C) 2023 Tristan Laan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime
import random
import requests
import time

cache = dict()

def _get_energy_price(from_date: datetime.datetime, to_date: datetime.datetime):
    # Wait between 0.1-0.2 seconds
    time.sleep((0.5 + random.random()) / 5)
    response = requests.get(f"https://api.energyzero.nl/v1/energyprices?"
                            f"&fromDate={from_date.isoformat()}"
                            f"&tillDate={to_date.isoformat()}"
                            "&interval=4&usageType=1&inclBtw=true")
    response.raise_for_status()

    data = response.json()
    price_per_hour = dict()
    for price_data in data['Prices']:
        date = datetime.datetime.fromisoformat(price_data['readingDate'])
        price = float(price_data['price']) + 0.17
        price_per_hour[date] = price

    return price_per_hour

def get_energy_price(from_date: datetime.datetime, to_date: datetime.datetime):
    if (from_date, to_date) not in cache:
        cache[(from_date, to_date)] = _get_energy_price(from_date, to_date)

    return cache[(from_date, to_date)]
