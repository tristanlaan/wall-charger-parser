# Alfen Eve wall charger log parser
# Copyright (C) 2023 Tristan Laan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import re
import datetime


def get_month(month: str) -> int:
    """Convert month string to month number regardless of current locale set in Python"""
    match month:
        case "Jan":
            return 1
        case "Feb":
            return 2
        case "Mar":
            return 3
        case "Apr":
            return 4
        case "May":
            return 5
        case "Jun":
            return 6
        case "Jul":
            return 7
        case "Aug":
            return 8
        case "Sep":
            return 9
        case "Oct":
            return 10
        case "Nov":
            return 11
        case "Dec":
            return 12


def parse_time(time: str, utc: bool = False) -> datetime.datetime:
    """Parses date string regardless of current locale set in Python"""
    pattern = r"(\w{3}) (\w{3}) (\d{2}) (\d{4}) (\d{2}):(\d{2}):(\d{2}) " \
              r"GMT([+-]\d{4})"
    match = re.match(pattern, time)
    if match:
        _weekday, month, day, year, hour, minute, second, tz_offset \
            = match.groups()
    else:
        raise RuntimeError(
            f"Datetime {time} does not follow pattern {pattern}")

    tz_hour = abs(int(tz_offset)) / 100
    tz_min = abs(int(tz_offset)) % 100
    tz_sign = +1 if int(tz_offset) >= 0 else -1
    tz = datetime.timezone(
        tz_sign * datetime.timedelta(hours=tz_hour, minutes=tz_min))

    local_time = datetime.datetime(
        int(year),
        get_month(month),
        int(day),
        int(hour),
        int(minute),
        int(second),
        tzinfo=tz)
    if utc:
        return local_time.astimezone(datetime.UTC)
    else:
        return local_time

def parse_timedelta(timedelta: str):
    pattern = r"(\d{2}):(\d{2}):(\d{2})"
    match = re.match(pattern, timedelta)
    if match:
        hours, minutes, seconds = match.groups()
    else:
        raise RuntimeError(
            f"Timedelta {timedelta} does not follow pattern {pattern}")
    return datetime.timedelta(hours=int(hours), minutes=int(minutes), seconds=int(seconds))
